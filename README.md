#### Transport APIs Aggregator

Query format:

`https://IP//api/transport?locale=ro&paxConfig=1_0_0&filterTransportTypes=train&debugging=true&to=Iasi&goDepartureDt=20-08-2020T07:12&username=emotional-player-19&from=Bucuresti&disableCache=true`

#### Database migrations interaction via Phalon aliases
```
alias phalcon_migrations_list='docker exec -it licenta_backend_php bash -c "cd src && ../vendor/bin/phalcon-migrations list"'
alias phalcon_migrations_generate='docker exec -it licenta_backend_php bash -c "cd src && ../vendor/bin/phalcon-migrations generate"'
alias phalcon_migrations_run='docker exec -it licenta_backend_php bash -c "cd src && ../vendor/bin/phalcon-migrations run"'
```

### The BA Thesis (the documentation of this application) is available [here](./Documentation-BA-Thesis.pdf).
