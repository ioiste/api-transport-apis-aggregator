# Get phalcon, fetch latest packages, install extensions for PDO, PgSQL, Memcached, Zip, clean apt
FROM mileschou/phalcon:7.4-apache AS phalcon
RUN apt-get update &&\
    apt-get install -y libpq-dev libmemcached-dev libzip-dev zlib1g-dev zip unzip --no-install-recommends &&\
    pecl install memcached-3.1.5 &&\
    docker-php-ext-install pdo pdo_pgsql zip &&\
    docker-php-ext-enable memcached &&\
    apt-get clean autoremove &&\
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Apache settings
FROM phalcon AS apache
ENV TZ=Europe/Bucharest
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone &&\
    echo "ServerName localhost" >> /etc/apache2/apache2.conf &&\
    a2enmod rewrite &&\
    service apache2 restart

# Install composer
FROM apache AS composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# For prod copy app files and run composer install
FROM composer AS app-build-prod
COPY . /var/www/html
RUN composer install --no-dev --classmap-authoritative --no-suggest
