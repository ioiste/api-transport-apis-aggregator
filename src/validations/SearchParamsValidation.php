<?php

namespace App\Validations;

use Phalcon\Validation;

/**
 * Class SearchParamsValidation
 *
 * @package App\Services
 */
class SearchParamsValidation extends Validation
{
    /**
     * @var string Validation errors messages
     */
    private const MESSAGE_DATE_FORMAT_WRONG =
        'The field :field has an invalid DateTime format. Expected format: DATE_RFC3339 (d-m-Y\TH:i).';
    private const MESSAGE_PAXCONFIG_WRONG =
        'The field :field has an invalid format. Expected: at least one adult and in the format AdultsNumber_ChildrenNumber_InfantsNumber. Adults + Children + Infants can\'t be grater than 9.';
    private const MESSAGE_FILTER_TRANSPORT_TYPES_WRONG =
        'The field :field contains unknown parameter(s). Expected values separated by comma (,): ';
    private const MESSAGE_LOCALE_WRONG =
        'The field :field contains unknown parameter(s). Expected values (one of): ';
    private const MESSAGE_FIELD_REQUIRED = 'The field :field is required.';

    /**
     * @var string Date format expected
     */
    private const DATE_FORMAT = 'd-m-Y\TH:i';

    /**
     * @var string Transport types that can be filtered
     */
    private const FILTER_TRANSPORT_TYPES_PARAMS = [
        'aircraft',
        'bus',
        'train',
        'public_transport',
    ];

    /**
     * @var string Locale field values
     */
    private const LOCALE_PARAMS = [
        'ro',
        'en',
    ];

    /**
     * Validation class init method
     */
    public function initialize(): void
    {
        $this->add(
            'from',
            new Validation\Validator\PresenceOf(
                [
                    'message' => self::MESSAGE_FIELD_REQUIRED
                ]
            )
        );
        $this->add(
            'goDepartureDt',
            new Validation\Validator\Callback(
                [
                    'callback' => static function ($data) {
                        $d = \DateTime::createFromFormat(self::DATE_FORMAT, $data['goDepartureDt']);
                        return $d && $d->format(self::DATE_FORMAT) === $data['goDepartureDt'];
                    },
                    'message' => self::MESSAGE_DATE_FORMAT_WRONG,
                ]
            )
        );
        $this->add(
            'backDepartureDt',
            new Validation\Validator\Callback(
                [
                    'callback' => static function ($data) {
                        $d = \DateTime::createFromFormat(self::DATE_FORMAT, $data['backDepartureDt']);
                        return $d && $d->format(self::DATE_FORMAT) === $data['backDepartureDt'];
                    },
                    'message' => self::MESSAGE_DATE_FORMAT_WRONG,
                    'allowEmpty' => true
                ]
            )
        );
        $this->add(
            'paxConfig',
            new Validation\Validator\Callback(
                [
                    'callback' => static function ($data) {
                        $paxConfigArray = explode('_', $data['paxConfig'], 3);
                        if (count($paxConfigArray) === 3) {
                            foreach ($paxConfigArray as $pax) {
                                if ($pax === '' || (int)$pax < 0) {
                                    return false;
                                }
                            }
                            return !((int)$paxConfigArray[0] <= 0) && array_sum($paxConfigArray) <= 9;
                        }
                        return false;
                    },
                    'message' => self::MESSAGE_PAXCONFIG_WRONG,
                ]
            )
        );
        $this->add(
            'filterTransportTypes',
            new Validation\Validator\Callback(
                [
                    'callback' => static function ($data) {
                        $filterTransportTypes = explode(',', trim($data['filterTransportTypes']));
                        foreach ($filterTransportTypes as $filterTransportType) {
                            if (!in_array($filterTransportType, self::FILTER_TRANSPORT_TYPES_PARAMS, true)) {
                                return false;
                            }
                        }
                        return true;
                    },
                    'message' => self::MESSAGE_FILTER_TRANSPORT_TYPES_WRONG .
                        implode(',', self::FILTER_TRANSPORT_TYPES_PARAMS),
                    'allowEmpty' => true
                ]
            )
        );
        $this->add(
            'locale',
            new Validation\Validator\Callback(
                [
                    'callback' => static function ($data) {
                        return in_array($data['locale'], self::LOCALE_PARAMS, true);
                    },
                    'message' => self::MESSAGE_LOCALE_WRONG .
                        implode(',', self::LOCALE_PARAMS),
                    'allowEmpty' => true
                ]
            )
        );
    }
}
