<?php

namespace App\Models;

use Phalcon\Mvc\Model;

/**
 * Class User
 *
 * @package App\Models
 */
class User extends Model
{
    /**
     * @var string
     */
    private string $first_name;

    /**
     * @var string
     */
    private string $last_name;

    /**
     * @var string
     */
    private string $email;

    /**
     * @var string
     */
    private string $password;

    /**
     * Initialization function of the User model
     */
    public function initialize(): void
    {
        $this->hasOne(
            'id',
            SearchRequest::class,
            'user_id',
            [
                'alias' => 'SearchRequest',
                'reusable' => true,
            ]
        );
    }

    /**
     * Get the related SearchRequest entities
     *
     * @param array $queryOptions
     *
     * @return false|Model\Resultset\Simple|\Phalcon\Mvc\Phalcon\Mvc\Model\Resultset\Simple
     */
    public function getSearchRequests(array $queryOptions = [])
    {
        return $this->getRelated('SearchRequest', $queryOptions);
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName(string $first_name): void
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName(string $last_name): void
    {
        $this->last_name = $last_name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }
}
