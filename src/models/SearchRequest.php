<?php

namespace App\Models;

use Phalcon\Mvc\Model;

/**
 * Class SearchRequest
 *
 * @package App\Models
 */
class SearchRequest extends Model
{
    /**
     * @var string
     */
    protected string $from;

    /**
     * @var string
     */
    protected ?string $to;

    /**
     * @var string
     */
    protected string $go_departure_dt;

    /**
     * @var string
     */
    protected ?string $back_departure_dt;

    /**
     * @var string
     */
    protected string $pax_config;

    /**
     * @var int|null
     */
    protected ?int $user_id;

    /**
     * Initialization function of the SearchRequest model
     */
    public function initialize(): void
    {
        $this->hasMany(
            'id',
            SearchResult::class,
            'search_request_id',
            [
                'alias' => 'SearchResult',
                'reusable' => true,
                'foreignKey' => [
                    'action' => Model\Relation::ACTION_CASCADE
                ]
            ]
        );
        $this->belongsTo(
            'user_id',
            User::class,
            'id',
            [
                'alias' => 'User',
                'reusable' => true,
            ]
        );
    }

    /**
     * Get the related SearchResult entities
     *
     * @param array $queryOptions
     *
     * @return false|Model\Resultset\Simple|\Phalcon\Mvc\Phalcon\Mvc\Model\Resultset\Simple
     */
    public function getSearchResults(array $queryOptions = [])
    {
        return $this->getRelated('SearchResult', $queryOptions);
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from): void
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo(?string $to): void
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getGoDepartureDt(): string
    {
        return $this->go_departure_dt;
    }

    /**
     * @param string $go_departure_dt
     */
    public function setGoDepartureDt(string $go_departure_dt): void
    {
        $this->go_departure_dt = $go_departure_dt;
    }

    /**
     * @return string
     */
    public function getBackDepartureDt(): string
    {
        return $this->back_departure_dt;
    }

    /**
     * @param string $back_departure_dt
     */
    public function setBackDepartureDt(?string $back_departure_dt): void
    {
        $this->back_departure_dt = $back_departure_dt;
    }

    /**
     * @return string
     */
    public function getPaxConfig(): string
    {
        return $this->pax_config;
    }

    /**
     * @param string $pax_config
     */
    public function setPaxConfig(string $pax_config): void
    {
        $this->pax_config = $pax_config;
    }

    /**
     * @return false|Model\Resultset\Simple|\Phalcon\Mvc\Phalcon\Mvc\Model\Resultset\Simple
     */
    public function getUser()
    {
        return $this->getRelated('User');
    }

    /**
     * @param User $user
     */
    public function setUser(?User $user): void
    {
        $this->user_id = !empty($user) ? $user->id : null;
    }

}
