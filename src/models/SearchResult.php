<?php

namespace App\Models;

use Phalcon\Mvc\Model;

/**
 * Class SearchResult
 *
 * @package App\Models
 */
class SearchResult extends Model
{
    /**
     * @var int
     */
    protected int $search_request_id;

    /**
     * @var string
     */
    protected string $source_api;

    /**
     * @var string
     */
    protected string $unique_key;

    /**
     * @var string
     */
    protected string $travel_duration;

    /**
     * @var string
     */
    protected string $departure_timestamp;

    /**
     * @var string
     */
    protected string $departure_place;

    /**
     * @var string
     */
    protected string $arrival_timestamp;

    /**
     * @var string
     */
    protected string $arrival_place;

    /**
     * @var string
     */
    protected string $transport_types;

    /**
     * @var float
     */
    protected ?float $price;

    /**
     * @var string
     */
    protected ?string $currency;

    /**
     * @var array|string
     */
    protected $segments;

    /**
     * @var string
     */
    protected string $info;

    /**
     * @var string
     */
    protected string $error;

    /**
     * Initialization function of the SearchResult model
     */
    public function initialize(): void
    {
        $this->belongsTo(
            'search_request_id',
            SearchRequest::class,
            'id',
            [
                'alias' => 'SearchRequest',
                'reusable' => true,
            ]
        );
    }

    /**
     * Get the related SearchRequest entity
     *
     * @param array $queryOptions
     *
     * @return false|Model\Resultset\Simple|\Phalcon\Mvc\Phalcon\Mvc\Model\Resultset\Simple
     */
    public function getSearchRequest(array $queryOptions = [])
    {
        return $this->getRelated('SearchRequest', $queryOptions);
    }

    /**
     * @param SearchRequest $searchRequest
     */
    public function setSearchRequest(SearchRequest $searchRequest): void
    {
        $this->search_request_id = $searchRequest->id;
    }

    /**
     * @return string
     */
    public function getSourceApi(): string
    {
        return $this->source_api;
    }

    /**
     * @param string $source_api
     */
    public function setSourceApi(string $source_api): void
    {
        $this->source_api = $source_api;
    }

    /**
     * @return string
     */
    public function getUniqueKey(): string
    {
        return $this->unique_key;
    }

    /**
     * @param string $unique_key
     */
    public function setUniqueKey(string $unique_key): void
    {
        $this->unique_key = $unique_key;
    }

    /**
     * @return int
     */
    public function getTravelDuration(): int
    {
        return $this->travel_duration;
    }

    /**
     * @param string $travel_duration
     */
    public function setTravelDuration(string $travel_duration): void
    {
        $this->travel_duration = $travel_duration;
    }

    /**
     * @return string
     */
    public function getDeparturePlace(): string
    {
        return $this->departure_place;
    }

    /**
     * @param string $departure_place
     */
    public function setDeparturePlace(string $departure_place): void
    {
        $this->departure_place = $departure_place;
    }

    /**
     * @return string
     */
    public function getDepartureTimestamp(): string
    {
        return $this->departure_timestamp;
    }

    /**
     * @param string $departure_timestamp
     */
    public function setDepartureTimestamp(string $departure_timestamp): void
    {
        $this->departure_timestamp = $departure_timestamp;
    }


    /**
     * @return string
     */
    public function getArrivalTimestamp(): string
    {
        return $this->arrival_timestamp;
    }

    /**
     * @param string $arrival_timestamp
     */
    public function setArrivalTimestamp(string $arrival_timestamp): void
    {
        $this->arrival_timestamp = $arrival_timestamp;
    }

    /**
     * @return string
     */
    public function getArrivalPlace(): string
    {
        return $this->arrival_place;
    }

    /**
     * @param string $arrival_place
     */
    public function setArrivalPlace(string $arrival_place): void
    {
        $this->arrival_place = $arrival_place;
    }

    /**
     * @return string
     */
    public function getTransportTypes(): string
    {
        return $this->transport_types;
    }

    /**
     * @param string $transport_types
     */
    public function setTransportTypes(string $transport_types): void
    {
        $this->transport_types = $transport_types;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo(string $info): void
    {
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function getSegments(): string
    {
        return $this->segments;
    }

    /**
     * @param array $segments
     */
    public function setSegments(array $segments): void
    {
        $this->segments = json_encode($segments);
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    public function setError(string $error): void
    {
        $this->error = $error;
    }

    /**
     * @param array $segments
     *
     * @return string
     */
    public static function createResultUniqueKey(array $segments): string
    {
        $lastSegmentKey = array_key_last($segments['go']);
        return str_replace(
            ' ', '',
            !empty($segments['go'][0]) ?
                $segments['go'][0]['departureTimestamp'] . '-' .
                $segments['go'][0]['departurePlace'] . '-' .
                $segments['go'][$lastSegmentKey]['arrivalTimestamp'] . '-' .
                $segments['go'][$lastSegmentKey]['arrivalPlace'] :
                uniqid('', true)
        );
    }
}
