<?php

namespace App\Middlewares;

use App\Services\APIs\ApiException;
use Phalcon\Events\Event;
use Phalcon\Http\Response;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

/**
 * NotFoundMiddleware
 *
 * @property Response $response
 */
class NotFoundMiddleware implements MiddlewareInterface
{
    /**
     * @param Event $event
     * @param Micro $application
     *
     * @throws ApiException
     */
    public function beforeNotFound(Event $event, Micro $application)
    {
        throw new ApiException('Not Found', 404);
    }

    /**
     * @param Micro $application
     *
     * @return bool
     */
    public function call(Micro $application): bool
    {
        return true;
    }
}