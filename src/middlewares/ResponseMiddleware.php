<?php

namespace App\Middlewares;

use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

/**
 * Class ResponseMiddleware
 * @package App\Middlewares
 */
class ResponseMiddleware implements MiddlewareInterface
{
    /**
     * @param Micro $application
     *
     * @return bool
     */
    public function call(Micro $application): bool
    {
        $return = $application->getReturnedValue();
        if (is_array($return)) {
            $application->response->setContent(json_encode($return ?? []));
        } else if ($return === '') {
            $application->response->setContent($return ?? '');

        }
        $application->response->send();
        return true;
    }
}
