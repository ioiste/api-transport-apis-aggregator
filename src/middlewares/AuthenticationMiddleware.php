<?php

namespace App\Middlewares;

use App\Services\APIs\ApiException;
use Phalcon\Events\Event;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

/**
 * Class AuthenticationMiddleware
 * @package App\Middlewares
 */
class AuthenticationMiddleware implements MiddlewareInterface
{
    /**
     * @param Event $event
     * @param Micro $application
     *
     * @return bool
     * @throws ApiException
     */
    public function beforeHandleRoute(
        Event $event,
        Micro $application
    ): bool
    {
        $config = $application->getDI()->get('config');
        $clients = $config->clients->toArray();

        $auth = $application->request->getHeader('Authorization');
        if (!empty($auth)) {
            $clientAuthData = explode('/', $auth);
            $clientName = substr($clientAuthData[0], strpos($clientAuthData[0], " ") + 1) ?? '';
            $clientKey = $clientAuthData[1] ?? '';

            if (!empty($clients[$clientName]) && $clientKey === $clients[$clientName]) {
                return true;
            }
        }
        $application->stop();
        throw new ApiException('Unauthorized', 401);
    }

    /**
     * @param Micro $application
     *
     * @return bool
     */
    public function call(Micro $application): bool
    {
        return true;
    }
}
