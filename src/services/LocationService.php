<?php

namespace App\Services;

use App\Services\APIs\SearchInterface;
use Phalcon\Config;
use Phalcon\Di\Injectable;

/**
 * Class LocationService
 *
 * @package App\Services
 */
class LocationService extends Injectable
{
    /**
     * @var string Service's type of APIs
     */
    public const LOCATION_TYPE = 'Location';

    /**
     * @var string Location type IATA
     */
    private const LOCATION_IATA = 'IATA';

    /**
     * @var string Location type Coordinates
     */
    private const LOCATION_COORDINATES = 'COORD';

    /**
     * @param string $city
     * @param string $conversionType
     *
     * @return array
     * @throws \Exception
     */
    public function convertCityTo(string $city, string $conversionType): array
    {
        $logger = $this->getDI()->get('logger');
        if (!in_array($conversionType, [self::LOCATION_IATA, self::LOCATION_COORDINATES])) {
            $logger->error('Invalid conversion type ' . $conversionType . ' in ' . __FUNCTION__);
            return [];
        }
        /** @var Config $config */
        $config = $this->getDI()->get('config');
        foreach ($config->get('APIs') as $apiName => $apiConfig) {
            [$apiName, $apiType] = explode('-', $apiName);
            if (
                $apiConfig['enabled'] &&
                $apiType === self::LOCATION_TYPE &&
                $apiConfig['locationsIdentifierType'] === $conversionType
            ) {
                $apiToQuery[$apiName] = $apiConfig->toArray();
                break;
            }
        }
        if (empty($apiToQuery)) {
            $logger->error('No location APIs found to query.');
            return [];
        }
        $apiSearchClass =
            '\\App\\Services\\APIs\\' . self::LOCATION_TYPE . '\\' . ucfirst(key($apiToQuery)) . '\\Search';
        /** @var SearchInterface $apiSearch */
        $params = ['city' => $city];
        $apiSearch = new $apiSearchClass(reset($apiToQuery), $params);
        $results = $apiSearch->perform();

        return $results ?? [];
    }
}
