<?php

namespace App\Services\APIs\Transport\Here;

use App\Services\APIs\AbstractParallelApi;

/**
 * Class Api
 *
 * @package App\Services\Kiwi
 */
class Api extends AbstractParallelApi
{
    /**
     * @var string API's name
     */
    protected string $name = 'Here-Transport';
}
