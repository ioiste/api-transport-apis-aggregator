<?php

namespace App\Services\APIs\Transport\Here;

use App\Models\SearchResult;
use App\Services\APIs\SearchInterface;

/**
 * Class Search
 *
 * @package App\Services\Here
 */
class Search extends Api implements SearchInterface
{
    /**
     * @var string Defines which section attributes are additionally requested
     */
    protected const ADDITIONAL_SECTION_ATTRIBUTES = 'intermediate,travelSummary,fares';

    /**
     * @var int For GO only: number of alternative routes to return aside from the optimal route.
     */
    protected const MAX_ALTERNATIVE_ROUTES_NB = 6;

    /**
     * @var int For BACK only: number of alternative routes to return aside from the optimal route.
     */
    protected const MIN_ALTERNATIVE_ROUTES_NB = 0;

    /**
     * @var array Transit modes which won't be in included in the final parsed response
     */
    protected const FILTERED_SECTION_TYPES = ['pedestrian'];

    /**
     * @var array Parameters which are required to be present in the received query
     */
    protected array $requiredParams = [
        'from',
        'to'
    ];

    /**
     * @var array|bool[] Vehicle types to request to be filtered
     */
    protected array $filterVehicleTypes = [
        'highSpeedTrain' => false,
        'intercityTrain' => false,
        'interRegionalTrain' => false,
        'regionalTrain' => false,
        'cityTrain' => false,
        'bus' => false,
        'ferry' => false,
        'subway' => false,
        'lightRail' => false,
        'privateBus' => false,
        'inclined' => false,
        'aerial' => false,
        'busRapid' => false,
        'monorail' => false,
        'flight' => false,
        'spaceship' => false,
    ];

    /**
     * @param array $response
     *
     * @return array|string[]
     * @throws \Exception
     */
    public function getParsedResponse(array $response): array
    {
        if ($this->debugging) {
            $parsedResponse['_debugging'] = $response['_debugging'];
        }
        foreach ($response['routes'] as $route) {
            $parsedSegments = $this->getSegments($route['sections']);
            $travelDuration = $this->getTravelDuration($parsedSegments);
            $parsedResponse[SearchResult::createResultUniqueKey($parsedSegments)] = [
                'segments' => $parsedSegments,
                'travelDuration' => $travelDuration,
                'departureTimestamp' => $parsedSegments['go'][0]['departureTimestamp'],
                'departurePlace' => $parsedSegments['go'][0]['departurePlace'],
                'arrivalTimestamp' => $parsedSegments['go'][0]['arrivalTimestamp'],
                'arrivalPlace' => $parsedSegments['go'][0]['arrivalPlace'],
                'price' => null,
                'currency' => null,
                'dataInfo' => [
                    'source' => $this->name,
                    'timestamp' => time()
                ],
            ];
        }

        return $parsedResponse ?? [];
    }

    /**
     * @param array $sections
     *
     * @return array
     * @throws \Exception
     */
    private function getSegments(array $sections): array
    {
        $segmentsGo = $this->getSegmentsGo($sections);
        $segmentsBack = $this->getSegmentsBack($segmentsGo);
        unset($segmentsGo['backRequestDetails']);

        return [
            'go' => $segmentsGo,
            'back' => $segmentsBack,
        ];
    }

    /**
     * @param array $sections
     *
     * @return array
     */
    private function getSegmentsGo(array $sections): array
    {
        $duration = 0;
        foreach ($sections as $key => $section) {
            if ($key === array_key_first($sections)) {
                $backRequestDetails['to'] = $section['departure']['place']['location']['lat'] . ',' .
                    $section['departure']['place']['location']['lng'];
            }
            if ($key === array_key_last($sections)) {
                $backRequestDetails['from'] = $section['arrival']['place']['location']['lat'] . ',' .
                    $section['arrival']['place']['location']['lng'];
            }
            $duration += $section['travelSummary']['duration'];
            if (!in_array(strtolower($section['type']), self::FILTERED_SECTION_TYPES)) {
                $segmentsGo[] = $this->parseSection($section);
            }
        }
        $segmentsGo['duration'] = $duration;
        $segmentsGo['backRequestDetails'] = $backRequestDetails ?? [];

        return $segmentsGo ?? [];
    }

    /**
     * @param array $section
     *
     * @return array
     */
    private function parseSection(array $section): array
    {
        $parsedSection = [
            'departurePlace' => $section['departure']['place']['name'],
            'departurePlaceType' => $section['departure']['place']['type'],
            'arrivalPlace' => $section['arrival']['place']['name'],
            'arrivalPlaceType' => $section['arrival']['place']['type'],
            'company' => $section['agency']['name'],
            'transportNo' => $section['transport']['name'],
            'transportType' => $section['transport']['mode'],
            'stops' => $this->getSectionStops($section['intermediateStops'])
        ];
        return array_merge(
            $this->getDatesFormatted($section),
            $parsedSection,
        );
    }

    /**
     * @param array $intermediateStops
     *
     * @return int
     */
    private function getSectionStops(array $intermediateStops): int
    {
        return count($intermediateStops);
    }

    /**
     * @param array $section
     *
     * @return array|int[]
     */
    private function getDatesFormatted(array $section): array
    {
        foreach (['arrival', 'departure'] as $boundWay) {
            $dTime = \DateTime::createFromFormat(DATE_RFC3339, $section[$boundWay]['time']);
            ${$boundWay . 'Timestamp'} = $dTime->getTimestamp();
        }

        return [
            'departureTimestamp' => $departureTimestamp ?? 0,
            'arrivalTimestamp' => $arrivalTimestamp ?? 0,
        ];
    }

    /**
     * @param array $segmentsGo
     *
     * @return array
     * @throws \Exception
     */
    private function getSegmentsBack(array $segmentsGo): array
    {
        $duration = 0;
        if (
            !empty($backRequestParams = $segmentsGo['backRequestDetails']) &&
            !empty($this->requestParams['backDepartureDt'])
        ) {
            $backRequestParams['goDepartureDt'] = $this->requestParams['backDepartureDt'];
            $searchParams = $this->buildSearchParams($backRequestParams, true);
            $responseBack = $this->getCall($searchParams);
            foreach ($responseBack['routes'] as $route) {
                foreach ($route['sections'] as $section) {
                    $duration += $section['travelSummary']['duration'];
                    if (!in_array(strtolower($section['type']), self::FILTERED_SECTION_TYPES)) {
                        $segmentsBack[] = $this->parseSection($section);
                    }
                }
                $segmentsBack['duration'] = $duration;
            }
            if ($this->debugging) {
                $segmentsBack['_debugging'] = $responseBack['_debugging'] ?? [];
            }
        }

        return $segmentsBack ?? [];
    }

    /**
     * @param array $requestParams
     *
     * @param bool $isBack
     *
     * @return array
     * @throws \Exception
     */
    public function buildSearchParams(array $requestParams = [], bool $isBack = false): array
    {
        $queryParams = empty($requestParams) ? $this->requestParams : $requestParams;
        $this->checkRequiredParams();
        $searchParams = [
            'apikey' => $this->apikey,
            'origin' => $queryParams['from'],
            'destination' => $queryParams['to'],
            'departureTime' => $this->getFormattedDate($queryParams['goDepartureDt'] ?? ''),
            'return' => self::ADDITIONAL_SECTION_ATTRIBUTES,
            'alternatives' => $isBack ? self::MIN_ALTERNATIVE_ROUTES_NB : self::MAX_ALTERNATIVE_ROUTES_NB,
            'modes' => $this->getVehicleTypes($queryParams),
            'lang' => $this->getLocale(),
        ];
        $this->unsetEmptyParams($searchParams);

        return $searchParams;
    }

    /**
     * @param string $departureDate
     *
     * @return string
     * @throws \Exception
     */
    private function getFormattedDate(string $departureDate): string
    {
        if (!empty($departureDate)) {
            $departureDate = new \DateTime($departureDate);
            $formattedDepartureDate = $departureDate->format(DATE_RFC3339);
        }

        return $formattedDepartureDate ?? '';
    }

    /**
     * @param array $queryParams
     *
     * @return string
     */
    private function getVehicleTypes(array $queryParams): ?string
    {
        if (!empty($queryParams['filterTransportTypes'])) {
            foreach (explode(',', $queryParams['filterTransportTypes']) as $filteredTransportType) {
                switch ($filteredTransportType):
                case 'aircraft':
                    $this->markVehicleTypesAsFiltered(['flight']);
                    break;
                case 'bus':
                    $this->markVehicleTypesAsFiltered(['bus', 'privateBus', 'busRapid']);
                    break;
                case 'train':
                    $this->markVehicleTypesAsFiltered(
                        [
                            'highSpeedTrain',
                            'intercityTrain',
                            'interRegionalTrain',
                            'regionalTrain',
                            'cityTrain'
                        ]
                    );
                    break;
                case 'public_transport':
                    $this->markVehicleTypesAsFiltered(
                        [
                            'flight',
                            'privateBus',
                            'busRapid',
                            'intercityTrain',
                            'interRegionalTrain',
                            'regionalTrain',
                            'highSpeedTrain'
                        ],
                        true
                    );
                    break;
                default:
                    break;
                endswitch;
            }
        }
        $vehicleTypesMarked = array_keys(array_filter($this->filterVehicleTypes));

        return !empty($vehicleTypesMarked) ? '-' . implode(',-', $vehicleTypesMarked) : null;
    }

    /**
     * @param array $vehicleTypes
     * @param bool $unfilter
     */
    private function markVehicleTypesAsFiltered(array $vehicleTypes, bool $unfilter = false): void
    {
        foreach ($this->filterVehicleTypes as $filterVehicleType => $isFiltered) {
            if (!$unfilter) {
                $filteringCondition = in_array($filterVehicleType, $vehicleTypes, true);
            } else {
                $filteringCondition = !in_array($filterVehicleType, $vehicleTypes, true);

            }
            if ($filteringCondition) {
                $this->filterVehicleTypes[$filterVehicleType] = true;
            }
        }
    }

    /**
     * @return string|null
     */
    private function getLocale(): ?string
    {
        $locale = null;
        if (!empty($this->requestParams['locale'])) {
            switch ($this->requestParams['locale']) {
            case 'ro':
                $locale = 'ro-RO';
                break;
            case 'en':
                $locale = 'en-US';
                break;
            default:
                break;
            }
        }

        return $locale;
    }

    /**
     * @param array $segments
     *
     * @return string
     */
    private function getTravelDuration(array &$segments): string
    {
        $totalDuration = 0;
        foreach (['go', 'back'] as $way) {
            $totalDuration += $segments[$way]['duration'] ?? 0;
            if (!empty($segments[$way]['duration'])) {
                unset($segments[$way]['duration']);
            }
        }
        if ($totalDuration < 24 * 60 * 60) {
            return gmdate('H\h i\m', $totalDuration);
        }
        $hours = floor($totalDuration / 3600);
        $minutes = floor(($totalDuration - $hours * 3600) / 60);

        return $hours . 'h ' . $minutes . 'm';
    }

    /**
     * @return bool
     */
    public function shouldQuery(): bool
    {
        return true;
    }
}
