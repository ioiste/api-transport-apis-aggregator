<?php

namespace App\Services\APIs\Transport\Kiwi;

use App\Models\SearchResult;
use App\Services\APIs\SearchInterface;

/**
 * Class Search
 *
 * @package App\Services\Kiwi
 */
class Search extends Api implements SearchInterface
{
    /**
     * @var string Datetime date formatting
     */
    protected const DATE_FORMAT = 'd/m/Y';

    /**
     * @var string Datetime time formatting
     */
    protected const TIME_FORMAT = 'H:i';

    /**
     * @var string Sorting criteria for the results; other values: duration, quality, date
     */
    protected const RESULTS_SORTING_CRITERIA = 'price';

    /**
     * @var bool Sort the results ascending or descending
     */
    protected const RESULTS_SORTING_TYPE_ASCENDING = true;

    /**
     * @var array Parameters which are required to be present in the received query
     */
    protected array $requiredParams = [
        'from',
        'goDepartureDt',
    ];

    /**
     * @var array Vehicle types list to request
     */
    protected array $vehicleTypes = [
        'aircraft',
        'bus',
        'train',
    ];

    /**
     * @return array
     * @throws \Exception
     */
    public function buildSearchParams(): array
    {
        $this->checkRequiredParams();
        $searchParams = array_merge(
            [
                'fly_from' => $this->requestParams['from'],
                'fly_to' => $this->requestParams['to'],
                'vehicle_type' => $this->getVehicleTypes(),
                'sort' => self::RESULTS_SORTING_CRITERIA,
                'asc' => (int)self::RESULTS_SORTING_TYPE_ASCENDING,
                'partner' => $this->apikey,
                'locale' => $this->getLocale(),
                'v' => self::VERSION,
            ],
            $this->getDatesFormatted(
                $this->requestParams['goDepartureDt'],
                $this->requestParams['backDepartureDt'] ?? ''
            ),
            $this->getPassengers($this->requestParams['paxConfig'] ?? '')
        );
        $this->unsetEmptyParams($searchParams);

        return $searchParams;
    }

    /**
     * @return string
     */
    private function getVehicleTypes(): string
    {
        if (!empty($this->requestParams['filterTransportTypes'])) {
            $this->vehicleTypes =
                array_diff($this->vehicleTypes, explode(',', $this->requestParams['filterTransportTypes']));
        }

        return implode(',', $this->vehicleTypes);
    }

    /**
     * @return string|null
     */
    private function getLocale(): ?string
    {
        return $this->requestParams['locale'] ?? null;
    }

    /**
     * @param string $goDepartureDt
     * @param string $backDepartureDt
     *
     * @return array
     * @throws \Exception
     */
    private function getDatesFormatted(string $goDepartureDt, string $backDepartureDt): array
    {
        $goDepartureDatetime = new \DateTime($goDepartureDt);
        $goDepartureDate = $goDepartureDatetime->format(self::DATE_FORMAT);
        if (!empty($backDepartureDt)) {
            $backDepartureDatetime = new \DateTime($backDepartureDt);
            $backDepartureDate = $backDepartureDatetime->format(self::DATE_FORMAT);

            $backDepartureDatesFormatted = [
                'return_from' => $backDepartureDate,
                'ret_dtime_from' => $backDepartureDatetime->format(self::TIME_FORMAT),
                'return_to' => $backDepartureDate,
            ];
        }

        return array_merge(
            [
                'date_from' => $goDepartureDate,
                'dtime_from' => $goDepartureDatetime->format(self::TIME_FORMAT),
                'date_to' => $goDepartureDate,
            ],
            $backDepartureDatesFormatted ?? []
        );
    }

    /**
     * @param string $paxConfig
     *
     * @return array
     */
    private function getPassengers(string $paxConfig): array
    {
        if (empty($paxConfig)) {
            return [];
        }

        [$adults, $children, $infants] = explode('_', $paxConfig);

        return [
            'adults' => $adults,
            'children' => $children,
            'infants' => $infants,
        ];
    }

    /**
     * @return bool
     */
    public function shouldQuery(): bool
    {
        return !empty($this->getVehicleTypes());
    }

    /**
     * @param array $response
     *
     * @return array|string[]
     */
    public function getParsedResponse(array $response): array
    {
        if ($this->debugging) {
            $parsedResponse['_debugging'] = array_merge($response['_debugging'], $response['search_params']);
        }
        $currency = $response['currency'];
        foreach ($response['data'] as $result) {
            $parsedSegments = $this->getSegments($result['route']);
            $parsedResponse[SearchResult::createResultUniqueKey($parsedSegments)] = [
                'segments' => $parsedSegments,
                'travelDuration' => $result['fly_duration'],
                'departureTimestamp' => $result['dTime'],
                'departurePlace' => $result['cityFrom'],
                'departureCountry' => $result['countryFrom']['name'],
                'arrivalTimestamp' => $result['aTime'],
                'arrivalPlace' => $result['cityTo'],
                'arrivalCountry' => $result['countryTo']['name'],
                'price' => $result['price'],
                'currency' => $currency,
                'deepLink' => $result['deep_link'],
                'dataInfo' => [
                    'source' => $this->name,
                    'timestamp' => time()
                ],
            ];
        }

        return $parsedResponse ?? [];
    }

    /**
     * @param array $routes
     *
     * @return array|array[]
     */
    private function getSegments(array $routes): array
    {
        foreach ($routes as $route) {
            ${'segments' . ($route['return'] === 0 ? 'Go' : 'Back')}[] = [
                'departureTimestamp' => $route['dTime'],
                'departurePlace' => $route['cityFrom'],
                'departureAirport' => $route['flyFrom'],
                'arrivalTimestamp' => $route['aTime'],
                'arrivalPlace' => $route['cityTo'],
                'arrivalAirport' => $route['flyTo'],
                'companyCode' => $route['airline'],
                'company' => $this->carriersService->getCarrierDataById($route['airline'])['name'],
                'transportNo' => $route['flight_no'],
                'transportType' => $route['vehicle_type'],
            ];
        }

        return [
            'go' => $segmentsGo ?? [],
            'back' => $segmentsBack ?? [],
        ];
    }
}
