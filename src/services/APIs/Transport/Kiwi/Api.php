<?php

namespace App\Services\APIs\Transport\Kiwi;

use App\Services\APIs\AbstractParallelApi;

/**
 * Class Api
 *
 * @package App\Services\Kiwi
 */
class Api extends AbstractParallelApi
{
    /**
     * @var string API's name
     */
    protected string $name = 'Kiwi-Transport';

    /**
     * @var int
     */
    protected const VERSION = 3;
}
