<?php

namespace App\Services\APIs\Location\Here;

use App\Services\APIs\SearchInterface;

/**
 * Class Search
 *
 * @package App\Services\Location\Here
 */
class Search extends Api implements SearchInterface
{
    /**
     * @var string How many results for the query term
     */
    private const LOCATION_OPTIONS_LIMIT = 1;
    /**
     * @var array|string[] Parameters which are required to be present in the received query
     */
    public array $requiredParams = [
        'city'
    ];

    /**
     * @return array
     * @throws \Exception
     */
    public function perform(): array
    {
        $params = $this->buildSearchParams();
        $response = $this->getCall($params);

        return $this->getParsedResponse($response);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function buildSearchParams(): array
    {
        $this->checkRequiredParams();
        return [
            'apikey' => $this->apikey,
            'q' => $this->requestParams['city'],
            'limit' => self::LOCATION_OPTIONS_LIMIT,
        ];
    }

    /**
     * @param array $response
     *
     * @return array
     */
    public function getParsedResponse(array $response): array
    {
        foreach ($response['items'] as $location) {
            if (!empty($location['position']['lat']) && !empty($location['position']['lng'])) {
                $parsedResponse['lat'] = $location['position']['lat'];
                $parsedResponse['lng'] = $location['position']['lng'];
            }
        }

        return $parsedResponse ?? [];
    }

    /**
     * @return bool
     */
    public function shouldQuery(): bool
    {
        return true;
    }
}
