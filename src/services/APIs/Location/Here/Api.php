<?php

namespace App\Services\APIs\Location\Here;

use App\Services\APIs\AbstractApi;

/**
 * Class Api
 *
 * @package App\Services\Location\Here
 */
class Api extends AbstractApi
{
    /**
     * @var string API's name
     */
    protected string $name = 'Here-Location';
}
