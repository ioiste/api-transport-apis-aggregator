<?php

namespace App\Services\APIs\Location\Kiwi;

use App\Services\APIs\SearchInterface;

/**
 * Class Search
 *
 * @package App\Services\Location\Kiwi
 */
class Search extends Api implements SearchInterface
{
    /**
     * @var string Location type that's about to be queried
     */
    private const LOCATION_TYPE_CITY = 'city';
    /**
     * @var string How many results for the query term
     */
    private const LOCATION_OPTIONS_LIMIT = 1;
    /**
     * @var array|string[] Parameters which are required to be present in the received query
     */
    public array $requiredParams = [
        'city'
    ];

    /**
     * @return array
     * @throws \Exception
     */
    public function perform(): array
    {
        $paramsCityInfo = $this->buildSearchParams();
        $responseCityInfo = $this->getCall($paramsCityInfo);

        return $this->getParsedResponse($responseCityInfo);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function buildSearchParams(): array
    {
        $this->checkRequiredParams();
        return [
            'term' => $this->requestParams['city'],
            'location_types' => self::LOCATION_TYPE_CITY,
            'limit' => self::LOCATION_OPTIONS_LIMIT,
            'active' => true,
        ];
    }

    /**
     * @param array $response
     *
     * @return array
     */
    public function getParsedResponse(array $response): array
    {
        foreach ($response['locations'] as $location) {
            if (!empty($location['code'])) {
                $parsedResponse[] = $location['code'];
            }
        }

        return $parsedResponse ?? [];
    }

    /**
     * @return bool
     */
    public function shouldQuery(): bool
    {
        return true;
    }
}
