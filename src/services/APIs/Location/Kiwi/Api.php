<?php

namespace App\Services\APIs\Location\Kiwi;

use App\Services\APIs\AbstractApi;

/**
 * Class Api
 *
 * @package App\Services\Location\Kiwi
 */
class Api extends AbstractApi
{
    /**
     * @var string API's name
     */
    protected string $name = 'Kiwi-Location';
}
