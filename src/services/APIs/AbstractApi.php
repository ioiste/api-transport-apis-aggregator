<?php

namespace App\Services\APIs;

use GuzzleHttp\Client;
use Phalcon\Di\Service;

/**
 * Class AbstractApi
 *
 * @package App\Services
 */
abstract class AbstractApi
{
    /**
     * @var string API Endpoint
     */
    protected string $endpoint;

    /**
     * @var string API Key
     */
    protected string $apikey;

    /**
     * @var bool Determines whether or not debugging data should be returned
     */
    protected bool $debugging;

    /**
     * @var bool Determines whether or not the response should be the mocked or live data
     */
    protected bool $shouldGetLiveData;

    /**
     * @var array API request params
     */
    protected array $requestParams;

    /**
     * Api constructor.
     *
     * @param array $config
     * @param array $params
     * @param array $services
     */
    public function __construct(array $config, array $params, array $services = [])
    {
        $this->endpoint = $config['endpoint'];
        $this->apikey = $config['apikey'];
        $this->requestParams = $params;
        $this->shouldGetLiveData = !!($this->requestParams['liveData'] ?? true);
        $this->debugging = (bool)($this->requestParams['debugging'] ?? null) && $this->shouldGetLiveData;
        if (!empty($services)) {
            foreach ($services as $serviceName => $serviceClone) {
                /** @var Service $serviceClone */
                $this->$serviceName = $serviceClone->resolve();
            }
        }
    }

    /**
     * @param array $queryParams
     *
     * @return mixed
     * @throws \Exception
     */
    public function getCall(array $queryParams)
    {
        if (!$this->shouldGetLiveData && ($mockedRs = $this->getMockedResponse())) {
            return $mockedRs;
        }
        $client = new Client();
        $uri = $this->endpoint . '?' . http_build_query($queryParams);
        $res = $client->request('GET', $uri);
        $response = json_decode($res->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        if ($this->debugging) {
            $response['_debugging'] = [
                '_uri' => $uri,
                '_statusCode' => $res->getStatusCode(),
            ];
        }

        return $response;
    }

    /**
     * @return array
     * @throws \JsonException
     */
    private function getMockedResponse(): array
    {
        switch ($this->name ?? '') {
            case 'Kiwi-Transport':
                $filename = 'kiwi-response.json';
                break;
            case 'Here-Transport':
                $filename = 'here-response.json';
                break;
            default:
                return [];
                break;
        }
        return json_decode(
            file_get_contents(dirname(__DIR__, 2) . '/assets/' . $filename),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
    }

    /**
     * @throws \Exception
     */
    protected function checkRequiredParams(): void
    {
        foreach ($this->requiredParams as $requiredParam) {
            if (empty($this->requestParams[$requiredParam])) {
                throw new \Exception(
                    $this->name . ': Missing or invalid required parameter: ' . $requiredParam,
                    '400'
                );
            }
        }
    }

    /**
     * @param array $searchParams
     */
    protected function unsetEmptyParams(array &$searchParams): void
    {
        foreach ($searchParams as $key => $param) {
            if ($searchParams[$key] === null) {
                unset($searchParams[$key]);
            }
        }
    }
}
