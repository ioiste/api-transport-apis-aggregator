<?php

namespace App\Services\APIs;

use Amp\Parallel\Worker\Environment;
use Amp\Parallel\Worker\Task;

abstract class AbstractParallelApi extends AbstractApi implements Task
{
    /**
     * Runs the task inside the caller's context.
     *
     * Does not have to be a coroutine, can also be a regular function returning a value.
     *
     * @param \Amp\Parallel\Worker\Environment
     *
     * @return mixed|\Amp\Promise|\Generator
     * @throws \Exception
     */
    public function run(Environment $environment)
    {
        $searchParams = $this->buildSearchParams();
        if (!$this->shouldQuery()) {
            return [];
        }
        $response = $this->getCall($searchParams);

        return $this->getParsedResponse($response);
    }

    /**
     * Search API entry point for non-parallel Search APIs
     * Declared only to comply with SearchInterface used in child Search classes implementing it
     *
     * @return array
     */
    public function perform(): array
    {
        return [];
    }
}
