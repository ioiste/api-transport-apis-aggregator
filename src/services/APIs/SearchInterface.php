<?php

namespace App\Services\APIs;

/**
 * Interface SearchInterface
 *
 * @package App\Services
 */
interface SearchInterface
{
    /**
     * Search API entry point for non-parallel Search APIs
     *
     * @return array
     */
    public function perform(): array;

    /**
     * @return array
     */
    public function buildSearchParams(): array;

    /**
     * @param array $response
     *
     * @return array
     */
    public function getParsedResponse(array $response): array;

    /**
     * @return bool
     */
    public function shouldQuery(): bool;
}
