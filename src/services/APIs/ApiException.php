<?php

namespace App\Services\APIs;

use Phalcon\Mvc\Micro\Exception;

/**
 * Class ApiException
 *
 * @package App\Services\APIs
 */
class ApiException extends Exception implements \JsonSerializable
{
    /**
     * Default problem type
     *
     * @see https://tools.ietf.org/html/rfc7807#section-4.2
     */
    private const DEFAULT_PROBLEM_TYPE = 'about:blank';

    /**
     * HTTP Response header
     *
     * @see https://tools.ietf.org/html/rfc7807#section-6.1
     */
    public const RESPONSE_HEADER = 'application/problem+json';

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $type;

    /**
     * @var mixed
     */
    private $detail;

    /**
     * @var string
     */
    private ?string $instance;

    /**
     * ApiException constructor.
     *
     * @param string $title
     * @param int    $status
     * @param mixed  $detail
     * @param string $type
     * @param string $instance
     */
    public function __construct(
        string $title,
        int $status,
        $detail = null,
        string $type = self::DEFAULT_PROBLEM_TYPE,
        ?string $instance = null
    ) {
        parent::__construct($title, $status);
        $this->title = $title;
        $this->type = $type;
        $this->detail = $detail;
        $this->instance = $instance;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return [
            'type' => $this->type,
            'title' => $this->title,
            'detail' => $this->detail,
            'instance' => $this->instance,
        ];
    }
}
