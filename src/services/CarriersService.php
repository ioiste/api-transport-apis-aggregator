<?php

namespace App\Services;

use Phalcon\Di\Injectable;

/**
 * Class Carriers
 *
 * @package App\Services
 */
class CarriersService extends Injectable
{
    /**
     * @var array|mixed
     */
    public array $list;

    /**
     * Carriers constructor.
     */
    public function __construct()
    {
        $this->list = json_decode(file_get_contents(APP_PATH . '/assets/carriers.json'), true);
    }

    /**
     * @param string $id
     *
     * @return array
     */
    public function getCarrierDataById(string $id): array
    {
        $key = array_search($id, array_column($this->list, 'id'));
        return $this->list[$key] ?? [];
    }
}
