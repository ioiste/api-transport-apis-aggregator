<?php

namespace App\Services;

use Amp\Parallel\Worker;
use Amp\Promise;
use App\Models\SearchRequest;
use App\Models\SearchResult;
use App\Models\User;
use App\Services\APIs\ApiException;
use App\Validations\SearchParamsValidation;
use Phalcon\Cache\Adapter\AdapterInterface;
use Phalcon\Config;
use Phalcon\Di\Injectable;
use Phalcon\Filter;
use Phalcon\Http\Request;
use Phalcon\Logger;

/**
 * Class TransportService
 *
 * @package App\Services
 */
class TransportService extends Injectable
{
    /**
     * @var string Service's type of APIs
     */
    public const TRANSPORT_TYPE = 'Transport';

    /**
     * @var array List of parameters read from the received query and their filter
     */
    public const PARAMS_FILTERS = [
        'from' => FILTER::FILTER_ALNUM,
        'to' => FILTER::FILTER_ALNUM,
        'goDepartureDt' => FILTER::FILTER_STRING,
        'backDepartureDt' => FILTER::FILTER_STRING,
        'paxConfig' => FILTER::FILTER_STRING,
        'filterTransportTypes' => FILTER::FILTER_STRING,
        'locale' => FILTER::FILTER_STRING,
        'debugging' => FILTER::FILTER_BOOL,
        'disableCache' => FILTER::FILTER_BOOL,
        'liveData' => FILTER::FILTER_BOOL,
        'username' => FILTER::FILTER_STRING,
    ];

    /**
     * @var Logger
     */
    protected Logger $logger;

    /**
     * @var SearchRequest|mixed|null
     */
    private SearchRequest $searchRequest;

    /**
     * @var mixed|null
     */
    private $transportDatabaseLogging;

    public function __construct()
    {
        $di = $this->getDI();
        $this->logger = $di->get('logger');
    }

    /**
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function getResults(Request $request): array
    {
        $params = $this->getParamsFromRequest($request);
        $this->transportDatabaseLogging = $this->getDI()->get('config')->application->transportDatabaseLogging;
        $this->logSearchRequest($params);
        $disableCache = $params['disableCache'];
        // Don't let the disableCache query param presence/absence trigger $cache->has($requestCacheKey) condition
        unset($params['disableCache']);
        $requestCacheKey = sha1(json_encode($params));
        /** @var AdapterInterface $cache */
        $cache = $this->getDI()->get('cache');
        if (!empty($disableCache) || !$cache->has($requestCacheKey)) {
            try {
                $results = $this->queryAPIs($params);
            } catch (\Throwable $exception) {
                $trace = $exception->getTraceAsString();
                if ($exception instanceof Worker\TaskFailureThrowable) {
                    $trace = $exception->getOriginalTraceAsString();
                }
                $this->logger->error('Error in: ' . __FUNCTION__ . ': ' . $exception->getMessage());
                $this->logger->error('Trace: ' . $trace);
                $results = [];
            }
            if (!empty($results)) {
                //TODO: make the logging in DB async from this function's return
                $this->logSearchResults($results);
                $cache->set($requestCacheKey, $results);
            }
            return $results ?? [];
        }
        return $cache->get($requestCacheKey);
    }

    /**
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    private function getParamsFromRequest(Request $request): array
    {
        foreach (self::PARAMS_FILTERS as $param => $filter) {
            $params[$param] = $request->getQuery($param, $filter);
        }
        $validation = new SearchParamsValidation();
        $failedValidations = $validation->validate($params);
        if (count($failedValidations)) {
            $failedValidationsMessages = [];
            foreach ($failedValidations as $failedValidation) {
                $failedValidationsMessages[] = $failedValidation->getMessage();
            }
            throw new ApiException('Bad Request', 400, $failedValidationsMessages);
        }

        return $params;
    }

    /**
     * @param array $requestParams
     */
    private function logSearchRequest(array $requestParams): void
    {
        if ($this->transportDatabaseLogging) {
            try {
                $searchRQ = new SearchRequest();
                $searchRQ->setFrom($requestParams['from']);
                $searchRQ->setTo($requestParams['to'] ?: null);
                $searchRQ->setGoDepartureDt($requestParams['goDepartureDt']);
                $searchRQ->setBackDepartureDt($requestParams['backDepartureDt'] ?: null);
                $searchRQ->setPaxConfig($requestParams['paxConfig']);
                $searchRQ->setUser($this->getUser($requestParams['username'] ?? null));
                if ($searchRQ->save() === false) {
                    $messages = $searchRQ->getMessages();
                    throw new \Exception(json_encode($messages));
                }
                $this->searchRequest = $searchRQ;
            } catch (\Throwable $throwable) {
                $this->logger->error('Error in ' . __FUNCTION__ . ': ' . $throwable->getMessage());
            }
        }
    }

    /**
     * @param string|null $username
     *
     * @return User|null
     * @throws \Exception
     */
    private function getUser(?string $username): ?User
    {
        if (empty($username)) {
            return null;
        }

        /** @var User $existingUser */
        $existingUser = User::findFirst([
            'email = "' . $username . '"'
        ]);
        if ($existingUser) {
            return $existingUser;
        }

        $firstLastName = explode('-', $username);
        $newUser = new User();
        $newUser->setEmail($username);
        $newUser->setFirstName($firstLastName[0]);
        $newUser->setLastName($firstLastName[1]);
        $newUser->setPassword(password_hash($username, PASSWORD_ARGON2ID));
        if ($newUser->save() === false) {
            $messages = $newUser->getMessages();
            throw new \Exception(json_encode($messages));
        }

        return $newUser;
    }

    /**
     * @param array $params
     * @return array
     *
     * @throws ApiException
     * @throws \Throwable
     */
    private function queryAPIs(array $params): array
    {
        /** @var Config $config */
        $config = $this->getDI()->get('config');
        foreach ($config->get('APIs') as $apiName => $apiConfig) {
            [$apiName, $apiType] = explode('-', $apiName);
            if ($apiConfig['enabled'] && $apiType === self::TRANSPORT_TYPE) {
                $apisToQuery[$apiName] = $apiConfig->toArray();
            }
        }
        if (empty($apisToQuery)) {
            $this->logger->error('No transport APIs found to query.');
            return [];
        }
        $originalParams = $params;
        $promises = [];
        foreach ($apisToQuery as $apiName => $apiConfig) {
            $apiSearchClass = '\\App\\Services\\APIs\\' . self::TRANSPORT_TYPE . '\\' . ucfirst($apiName) . '\\Search';
            $params = $this->convertLocationParams($params, $apiConfig['locationsIdentifiedBy']);
            $promises[] = Worker\enqueue(
                new $apiSearchClass(
                    $apiConfig,
                    $params,
                    [
                        'carriersService' => clone $this->getDI()->getService('carriers-service')
                    ]
                )
            );
            $params = $originalParams;
        }
        $results = Promise\wait(Promise\all($promises));

        return array_merge(...$results);
    }

    /**
     * @param array  $params
     * @param string $locationsIdentifiedBy
     *
     * @return array
     * @throws \Exception
     */
    private function convertLocationParams(array $params, string $locationsIdentifiedBy): array
    {
        /** @var LocationService $locationService */
        $locationService = $this->getDI()->get('location-service');
        $airportsFrom = $locationService->convertCityTo($params['from'], $locationsIdentifiedBy);
        if (!empty($airportsFrom)) {
            $params['from'] = implode(',', $airportsFrom);
            if (!empty($params['to'])) {
                $airportsTo = $locationService->convertCityTo($params['to'], $locationsIdentifiedBy);
                $params['to'] = implode(',', $airportsTo);
            }
        }

        return $params;
    }

    /**
     * @param array $searchResultsRaw
     */
    private function logSearchResults(array $searchResultsRaw): void
    {
        if ($this->transportDatabaseLogging) {
            try {
                foreach ($searchResultsRaw as $uniqueKey => $result) {
                    if ($uniqueKey !== '_debugging') {
                        $searchResult = new SearchResult();
                        $searchResult->setSearchRequest($this->searchRequest);
                        $searchResult->setSourceApi($result['dataInfo']['source']);
                        $searchResult->setUniqueKey($uniqueKey);
                        $searchResult->setTravelDuration($result['travelDuration']);
                        $searchResult->setDepartureTimestamp($result['departureTimestamp']);
                        $searchResult->setDeparturePlace($result['departurePlace']);
                        $searchResult->setArrivalTimestamp($result['arrivalTimestamp']);
                        $searchResult->setArrivalPlace($result['arrivalPlace']);
                        $searchResult->setTransportTypes(''); //TODO: (?) parse transport types
                        $searchResult->setPrice($result['price']);
                        $searchResult->setCurrency($result['currency']);
                        $searchResult->setSegments($result['segments']);
                        $searchResult->setInfo('{}'); //TODO: request info (response time, etc)
                        $searchResult->setError('{}'); //TODO: error details, if any
                        if ($searchResult->save() === false) {
                            $messages = $searchResult->getMessages();
                            throw new \Exception(json_encode($messages));
                        }
                    }
                }
            } catch (\Throwable $throwable) {
                $this->logger->error('Error in ' . __FUNCTION__ . ': ' . $throwable->getMessage());
            }
        }
    }
}
