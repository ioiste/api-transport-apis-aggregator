<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller;
use App\Services\TransportService;

/**
 * Class TransportController
 *
 * @package App\Controllers
 */
class TransportController extends Controller
{
    /**
     * @return array
     * @throws \Exception|\Throwable
     */
    public function index(): array
    {
        /** @var TransportService $transportService */
        $transportService = $this->getDI()->get('transport-service');

        return $transportService->getResults($this->request);
    }
}
