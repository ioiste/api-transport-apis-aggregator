<?php

use App\Middlewares\AuthenticationMiddleware;
use App\Middlewares\NotFoundMiddleware;
use App\Middlewares\ResponseMiddleware;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Events\Manager;

foreach ($routes as $routeName => $routeDefinition) {
    $route = $$routeName = new MicroCollection();
    $route
        ->setHandler($routeDefinition['handlerClass'])
        ->setLazy($routeDefinition['lazyLoading'])
        ->setPrefix($routeDefinition['prefix']);
    foreach ($routeDefinition['httpMethods'] as $httpMethod => $httpMethodDefinition) {
        $route->$httpMethod(
            $httpMethodDefinition['routePattern'],
            $httpMethodDefinition['handlerClassMethod'],
        );
    }
    $app->mount($route);
}

$manager = new Manager;
$manager->attach(
    'micro',
    new AuthenticationMiddleware()
);
$app->before(
    new AuthenticationMiddleware()
);
$manager->attach(
    'micro',
    new ResponseMiddleware()
);
$app->after(
    new ResponseMiddleware()
);
$manager->attach(
    'micro',
    new NotFoundMiddleware()
);
$app->notFound(
    new NotFoundMiddleware()
);

$app->error(
    function (Exception $exception) use ($app) {
        $app
            ->response
            ->setStatusCode($exception->getCode())
            ->setJsonContent($exception)
            ->setHeader('Content-Type', $exception::RESPONSE_HEADER)
            ->sendHeaders()
            ->send();
    }
);

$app->setEventsManager($manager);
