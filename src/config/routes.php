<?php

use App\Controllers\TransportController;

$routes = [
    'transport' => [
        'handlerClass' => TransportController::class,
        'lazyLoading' => true,
        'prefix' => '/transport',
        'httpMethods' => [
            'get' => [
                'routePattern' =>
                    '/?{from}?{to}?{goDepartureDt}?{backDepartureDt}?{paxConfig}',
                'handlerClassMethod' => 'index',
            ]
        ],
    ],
];
