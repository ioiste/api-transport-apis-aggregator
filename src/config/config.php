<?php

use Phalcon\Config;

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: dirname(__DIR__, 2) . '');
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/src');

return new Config(
    [
        'database' => [
            'adapter' => 'Postgresql',
            'host' => getenv('POSTGRES_HOST'),
            'username' => getenv('POSTGRES_USER'),
            'password' => getenv('POSTGRES_PASSWORD'),
            'schema' => 'public',
            'dbname' => getenv('POSTGRES_DB'),
        ],
        'caching' => [
            'host' => getenv('CACHING_HOST'),
            'port' => getenv('CACHING_PORT'),
            'defaultSerializer' => 'Json',
            'lifetime' => 3600,
        ],
        'application' => [
            'baseUri' => '/',
            'transportDatabaseLogging' => true,
        ],
        'APIs' => [
            'Kiwi-Transport' => [
                'enabled' => true,
                'endpoint' => 'https://api.skypicker.com/flights',
                'apikey' => getenv('KIWI_APIKEY'),
                'locationsIdentifiedBy' => 'IATA',
            ],
            'Here-Transport' => [
                'enabled' => true,
                'endpoint' => 'https://transit.router.hereapi.com/v8/routes',
                'apikey' => getenv('HERE_APIKEY'),
                'locationsIdentifiedBy' => 'COORD',
            ],
            'Kiwi-Location' => [
                'enabled' => true,
                'endpoint' => 'https://api.skypicker.com/locations',
                'apikey' => getenv('KIWI_APIKEY'),
                'locationsIdentifierType' => 'IATA',
            ],
            'Here-Location' => [
                'enabled' => true,
                'endpoint' => 'https://discover.search.hereapi.com/v1/geocode',
                'apikey' => getenv('HERE_APIKEY'),
                'locationsIdentifierType' => 'COORD',
            ],
        ],
        'clients' => [
            'web-app' => getenv('WEBAPP_APIKEY'),
        ]
    ]
);
