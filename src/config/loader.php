<?php

use Phalcon\Loader;

$loader = new Loader();
$loader->registerNamespaces(
    array_merge(
        [
            'App\Services' => dirname(__DIR__) . '/services/',
            'App\Controllers' => dirname(__DIR__) . '/controllers/',
            'App\Models' => dirname(__DIR__) . '/models/',
            'App\Middlewares' => dirname(__DIR__) . '/middlewares/',
            'App\Validations' => dirname(__DIR__) . '/validations/',
        ],
        (static function ($config) {
            foreach ($config->APIs as $apiName => $apiConfig) {
                [$apiName, $apiType] = explode('-', $apiName);
                if ($apiConfig['enabled']) {
                    $apisNamespaces[] = [
                        "App\Services\\$apiType\\$apiName" => dirname(__DIR__) . "/services/$apiType/$apiName"
                    ];
                }
            }
            return $apisNamespaces ?? [];
        })($config)
    )
);
$loader->register();
