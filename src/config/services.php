<?php
declare(strict_types=1);

use App\Services\CarriersService;
use App\Services\LocationService;
use App\Services\TransportService;
use Phalcon\Http\Response;
use Phalcon\Url as UrlResolver;
use Phalcon\Session\Adapter\Libmemcached;
use Phalcon\Session\Manager;
use Phalcon\Storage\AdapterFactory;
use Phalcon\Storage\SerializerFactory;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\Stream;

$di->setShared(
    'config', function () {
        return include APP_PATH . '/config/config.php';
    }
);

$di->setShared(
    'url', function () {
        $config = $this->getConfig();

        $url = new UrlResolver();
        $url->setBaseUri($config->application->baseUri);

        return $url;
    }
);

$di->setShared(
    'db', function () {
        $config = $this->getConfig();

        $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
        $params = [
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'charset' => $config->database->charset
        ];

        if ($config->database->adapter === 'Postgresql') {
            unset($params['charset']);
        }

        return new $class($params);
    }
);

$di->setShared(
    'cache', function () {
        $config = $this->getConfig();

        $options = [
        'client' => [],
        'servers' => [
            [
                'host' => $config->caching->host,
                'port' => $config->caching->port,
                'weight' => 0,
                'defaultSerializer' => $config->caching->defaultSerializer,
                'lifetime' => $config->caching->lifetime
            ],
        ],
        ];

        $cache = new Manager();
        $serializerFactory = new SerializerFactory();
        $factory = new AdapterFactory($serializerFactory);
        $memcached = new Libmemcached($factory, $options);

        $cache->setAdapter($memcached)->start();

        return $cache;
    }
);

/**
 * Overriding Response-object to set the Content-type header globally
 */
$di->setShared(
    'response', function () {
        $response = new Response();
        $response->setContentType('application/json', 'utf-8');

        return $response;
    }
);

$di->setShared(
    'transport-service', function () {
        return new TransportService();
    }
);

$di->setShared(
    'location-service', function () {
        return new LocationService();
    }
);

$di->setShared(
    'carriers-service', function () {
        return new CarriersService();
    }
);

$di->setShared(
    'logger', function () {
        $adapter = new Stream('php://stderr');
        return new Logger(
            'messages',
            [
            'main' => $adapter,
            ]
        );
    }
);
