ALTER DATABASE "transport_api_agg" SET TIMEZONE TO "Europe/Bucharest";
ALTER DATABASE "transport_api_agg" SET datestyle TO "ISO, DMY";

-- Table: public.user

-- DROP TABLE public."user";

CREATE TABLE public."user"
(
    id serial,
    first_name character varying COLLATE pg_catalog."default" NOT NULL,
    last_name character varying COLLATE pg_catalog."default" NOT NULL,
    email character varying COLLATE pg_catalog."default" NOT NULL,
    password character varying COLLATE pg_catalog."default" NOT NULL,
    disabled boolean NOT NULL DEFAULT false,
    created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT user_pk PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public."user"
    OWNER to postgres;


-- Table: public.search_request

-- DROP TABLE public.search_request;

CREATE TABLE public.search_request
(
    id serial,
    "from" character varying COLLATE pg_catalog."default" NOT NULL,
    "to" character varying COLLATE pg_catalog."default",
    go_departure_dt timestamp without time zone NOT NULL,
    back_departure_dt timestamp without time zone,
    pax_config character varying COLLATE pg_catalog."default" NOT NULL,
    created_at character varying COLLATE pg_catalog."default" NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_id integer,
    CONSTRAINT search_request_pkey PRIMARY KEY (id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE SET NULL
        ON DELETE SET NULL
)

    TABLESPACE pg_default;

ALTER TABLE public.search_request
    OWNER to postgres;

-- Table: public.search_result

-- DROP TABLE public.search_result;

CREATE TABLE public.search_result
(
    id serial,
    search_request_id integer NOT NULL,
    source_api character varying COLLATE pg_catalog."default",
    unique_key character varying COLLATE pg_catalog."default",
    travel_duration character varying COLLATE pg_catalog."default",
    departure_timestamp character varying COLLATE pg_catalog."default",
    departure_place character varying COLLATE pg_catalog."default",
    arrival_timestamp character varying COLLATE pg_catalog."default",
    arrival_place character varying COLLATE pg_catalog."default",
    transport_types character varying COLLATE pg_catalog."default",
    price real,
    currency character varying COLLATE pg_catalog."default",
    segments json,
    info json,
    error json,
    created_at character varying COLLATE pg_catalog."default" NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT search_result_pkey PRIMARY KEY (id),
    CONSTRAINT fk_search_request FOREIGN KEY (search_request_id)
        REFERENCES public.search_request (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

    TABLESPACE pg_default;

ALTER TABLE public.search_result
    OWNER to postgres;
