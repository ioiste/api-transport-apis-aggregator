<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Migrations\Mvc\Model\Migration;

/**
 * Class SearchResultMigration_100
 */
class SearchResultMigration_100 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable(
            'search_result', [
                'columns' => [
                    new Column(
                        'id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 1,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'search_request_id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 1,
                            'after' => 'id'
                        ]
                    ),
                    new Column(
                        'source_api',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'search_request_id'
                        ]
                    ),
                    new Column(
                        'unique_key',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'source_api'
                        ]
                    ),
                    new Column(
                        'travel_duration',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'unique_key'
                        ]
                    ),
                    new Column(
                        'departure_timestamp',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'travel_duration'
                        ]
                    ),
                    new Column(
                        'departure_place',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'departure_timestamp'
                        ]
                    ),
                    new Column(
                        'arrival_timestamp',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'departure_place'
                        ]
                    ),
                    new Column(
                        'arrival_place',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'arrival_timestamp'
                        ]
                    ),
                    new Column(
                        'transport_types',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'arrival_place'
                        ]
                    ),
                    new Column(
                        'price',
                        [
                            'type' => Column::TYPE_FLOAT,
                            'notNull' => false,
                            'after' => 'transport_types'
                        ]
                    ),
                    new Column(
                        'currency',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'price'
                        ]
                    ),
                    new Column(
                        'segments',
                        [
                            'type' => Column::TYPE_JSON,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'currency'
                        ]
                    ),
                    new Column(
                        'info',
                        [
                            'type' => Column::TYPE_JSON,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'segments'
                        ]
                    ),
                    new Column(
                        'error',
                        [
                            'type' => Column::TYPE_JSON,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'info'
                        ]
                    ),
                    new Column(
                        'created_at',
                        [
                            'type' => Column::TYPE_TIMESTAMP,
                            'default' => "CURRENT_TIMESTAMP",
                            'notNull' => false,
                            'after' => 'error'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('search_result_pkey', ['id'], '')
                ],
                'references' => [
                    new Reference(
                        'fk_search_request',
                        [
                            'referencedSchema' => 'transport_api_agg',
                            'referencedTable' => 'search_request',
                            'columns' => ['search_request_id'],
                            'referencedColumns' => ['id'],
                            'onUpdate' => 'NO ACTION',
                            'onDelete' => 'NO ACTION'
                        ]
                    )
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
