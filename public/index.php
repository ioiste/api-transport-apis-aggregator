<?php
declare(strict_types=1);
require '../vendor/autoload.php';

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/src');

try {
    $di = new FactoryDefault();
    include APP_PATH . '/config/services.php';
    $config = $di->getConfig();
    include APP_PATH . '/config/loader.php';
    $app = new Micro($di);
    include APP_PATH . '/config/routes.php';
    include APP_PATH . '/app.php';
    $app->handle($_SERVER['REQUEST_URI']);
} catch (\Throwable $t) {
    $di->get('logger')->critical('Something went wrong: ' . $t->getMessage());
    $di->get('logger')->critical('Trace: ' . $t->getTraceAsString());
}
